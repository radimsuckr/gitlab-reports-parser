import logging
from io import StringIO

import gitlab

from .exception import SyftAnchorBeginAfterEndError, SyftAnchorBeginNotFound, SyftAnchorEndNotFound, TokenError
from .model.matches import Match, Severity, Vulnerability

SYFT_ANCHOR_BEGIN = '[//]: #gitlab-report-anchor-syft-begin'
SYFT_ANCHOR_END = '[//]: #gitlab-report-anchor-syft-end'

logger = logging.getLogger(__name__)


def get_report_markdown(grouped_matches: dict[Severity, set[Match]]) -> str:
    def _write_matches(grouped_matches: dict[Severity, set[Match]]) -> None:
        def _write_vuln(buff: StringIO, vuln: Vulnerability) -> None:
            buff.write(f'<li>{vuln.id}</li>')
            if len(vuln.urls) > 0:
                buff.write('<ul>')
                for url in vuln.urls:
                    buff.write(f'<li>{url}</li>')
                buff.write('</ul>')

        for severity, matches in grouped_matches.items():
            buff.write('<details>')
            buff.write(
                f'<summary><strong>{severity.name.capitalize()} severity issues ({len(matches)})</strong></summary>'
            )
            buff.write('<ul>')
            for item in matches:
                _write_vuln(buff, item.vulnerability)
            buff.write('</ul>')
            buff.write('</details>')

    buff = StringIO()
    buff.write(f'\n\n{SYFT_ANCHOR_BEGIN}\n\n')  # IMPORTANT: Keep two lines before and after the comment

    buff.write('## Security report\n')
    # buff.write('### Negligible severity issues\n')
    # for crit_item in grouped_matches[Severity.NEGLIGIBLE]:
    #     _write_vuln(buff, crit_item.vulnerability)
    _write_matches(grouped_matches)

    buff.write(f'\n\n{SYFT_ANCHOR_END}\n\n')  # IMPORTANT: Keep two lines before and after the comment
    return buff.getvalue()


def update_merge_request_description(text: str, project_id: str, merge_request_iid: str,
                                     private_token: str = '') -> None:
    """
    This function will remove all lines in the merge request description between SYFT_ANCHOR_BEGIN and SYFT_ANCHOR_END
    lines and replace them with current security report.
    """
    if not private_token:
        raise TokenError('Set either private_token')
    client = gitlab.Gitlab(private_token=private_token)

    project = client.projects.get(project_id)
    logger.debug('Found project %s', project_id)
    mr_detail = project.mergerequests.get(merge_request_iid)
    logger.debug('Found merge request %s in project %s', merge_request_iid, project_id)
    lines = mr_detail.description.split('\n')
    try:
        anchor_begin_ix = lines.index(SYFT_ANCHOR_BEGIN)
    except ValueError:
        raise SyftAnchorBeginNotFound()
    logger.debug('Found Syft anchor beginning')
    try:
        anchor_end_ix = lines.index(SYFT_ANCHOR_END)
    except ValueError:
        raise SyftAnchorEndNotFound()
    logger.debug('Found Syft anchor ending')

    if anchor_begin_ix > anchor_end_ix:
        raise SyftAnchorBeginAfterEndError('Anchor begin is after anchor end')

    del lines[anchor_begin_ix:anchor_end_ix + 1]
    lines.append(text)
    updated_text = '\n'.join(lines)
    mr_detail.description = updated_text
    mr_detail.save()
