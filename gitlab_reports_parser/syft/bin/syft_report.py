import logging
import sys
from os import getenv

from gitlab_reports_parser.syft.exception import (
    SyftAnchorBeginAfterEndError, SyftAnchorBeginNotFound, SyftAnchorEndNotFound,
)
from gitlab_reports_parser.syft.gitlab import TokenError, get_report_markdown, update_merge_request_description
from gitlab_reports_parser.syft.loader import group_by_severities, load
from gitlab_reports_parser.syft.model.matches import Severity


def main():
    LOGGING_LEVEL = getenv('LOGGING_LEVEL', 'INFO')
    logging.basicConfig(
        level=LOGGING_LEVEL,
        format='%(asctime)s %(module)s %(levelname)s: %(message)s' if LOGGING_LEVEL == 'DEBUG' else '%(message)s',
    )
    logger = logging.getLogger(__name__)

    PRIVATE_TOKEN = getenv('PRIVATE_TOKEN', '')
    REPORT_PATH = getenv('REPORT_PATH', '')

    error_messages = []
    if not PRIVATE_TOKEN:
        error_messages.append('Provide PRIVATE_TOKEN environment variable')
    CI_PROJECT_ID = getenv('CI_PROJECT_ID')
    if not CI_PROJECT_ID:
        error_messages.append('Provide CI_PROJECT_ID environment variable')
    CI_MERGE_REQUEST_IID = getenv('CI_MERGE_REQUEST_IID')
    if not CI_MERGE_REQUEST_IID:
        error_messages.append('Provide CI_MERGE_REQUEST_IID environment variable')
    if not REPORT_PATH:
        error_messages.append('Provide REPORT_PATH environment variable')

    if len(error_messages) > 0:
        for msg in error_messages:
            logger.error(msg)
        sys.exit(1)

    content = load(REPORT_PATH)

    grouped = group_by_severities(content)
    logger.info(
        'Severity report:\nUnknown: %d\nNegligible: %d\nLow: %d\nMedium: %d\nHigh: %d\nCritical: %d',
        len(grouped[Severity.UNKNOWN]),
        len(grouped[Severity.NEGLIGIBLE]),
        len(grouped[Severity.LOW]),
        len(grouped[Severity.MEDIUM]),
        len(grouped[Severity.HIGH]),
        len(grouped[Severity.CRITICAL]),
    )

    report = get_report_markdown(grouped)
    try:
        update_merge_request_description(report, CI_PROJECT_ID, CI_MERGE_REQUEST_IID, private_token=PRIVATE_TOKEN)
    except TokenError as ex:
        logger.exception(ex)
    except SyftAnchorBeginNotFound as ex:
        logger.exception(ex)
        logger.error('Failed to find beginning of anchor for Syft report')
        sys.exit(100)
    except SyftAnchorEndNotFound as ex:
        logger.exception(ex)
        logger.error('Failed to find ending of anchor for Syft report')
        sys.exit(101)
    except SyftAnchorBeginAfterEndError as ex:
        logger.exception(ex)
        logger.error('Syft report anchor beginning is after anchor ending comment')
        sys.exit(102)
