from dataclasses import dataclass
from enum import Enum


class Severity(Enum):

    UNKNOWN = 'Unknown'
    NEGLIGIBLE = 'Negligible'
    LOW = 'Low'
    MEDIUM = 'Medium'
    HIGH = 'High'
    CRITICAL = 'Critical'


@dataclass
class Metrics:

    base_score: int
    exploitability_score: int
    impact_score: float


@dataclass
class CVS:

    version: str
    vector: str
    metrics: Metrics
    vendor_metadata: dict


class State(Enum):

    UNKNOWN = 'unknown'
    FIXED = 'fixed'
    NOT_FIXED = 'not-fixed'
    WONT_FIX = 'wont-fix'


@dataclass
class Fix:

    versions: list[str]
    state: State


@dataclass
class Vulnerability:

    id: str
    data_source: str
    namespace: str
    severity: Severity
    urls: list[str]
    cvss: list[CVS]
    fix: Fix
    advisories: list[str]
    description: str | None = None


@dataclass
class RelatedVulnerability:

    id: str
    data_source: str
    namespace: str
    severity: Severity
    urls: list[str]
    cvss: list[str]
    description: str


@dataclass
class Distro:

    type: str
    version: str


@dataclass
class Package:

    name: str
    version: str


@dataclass
class SearchedBy:

    namespace: str
    language: str | None = None
    cpes: list[str] | None = None
    distro: Distro | None = None
    package: Package | None = None


@dataclass
class Found:

    version_constraint: str | dict
    cpes: list[str] | None = None


@dataclass
class MatchDetail:

    type: str
    matcher: str
    searched_by: SearchedBy
    found: Found


@dataclass
class Location:

    path: str
    layer_id: str


@dataclass
class Upstream:

    name: str
    version: str | None = None


@dataclass
class Artifact:

    name: str
    version: str
    type: str
    locations: list[Location]
    language: str
    licenses: list[str]
    cpes: list[str]
    purl: str
    upstreams: list[Upstream]


@dataclass
class Match:

    vulnerability: Vulnerability
    related_vulnerabilities: list[RelatedVulnerability]
    match_details: list[MatchDetail]
    artifact: Artifact

    def __eq__(self, other):
        return self.vulnerability.id == other.vulnerability.id

    def __ne__(self, other):
        return self.vulnerability.id != other.vulnerability.id

    def __hash__(self):
        return hash(repr(self.vulnerability.id))
