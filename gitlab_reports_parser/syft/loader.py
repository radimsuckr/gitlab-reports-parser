import json

from dataclass_wizard import fromlist

from .model.matches import Match, Severity


def load(report_path: str) -> set[Match]:
    with open(report_path, 'r') as report:
        content = json.load(report)['matches']
        parsed = set(fromlist(Match, content))
    return parsed


def group_by_severities(matches: set[Match]) -> dict[Severity, set[Match]]:
    return {
        severity: set(filter(lambda x: x.vulnerability.severity == severity, matches))
        for severity in Severity.__members__.values()
    }
