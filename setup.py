from pathlib import Path

from setuptools import find_packages, setup


def read_root_file(filename):
    root = Path('.')
    with open(root / filename, 'r') as f:
        return f.read().strip()


setup(
    name='gitlab-reports-parser',
    entry_points={
        'console_scripts': [
            'syft-report = gitlab_reports_parser.syft.bin.syft_report:main',
        ],
    },
    author='Radim Sückr',
    author_email='kontakt@radimsuckr.cz',
    description='',
    long_description=read_root_file('readme.md'),
    long_description_content_type='text/markdown',
    license='MIT',
    # url='https://github.com/druids/celery-aws-xray-sdk-extension',
    packages=find_packages(),
    install_requires=[
        'dataclass-wizard>=0.22.1,<1',
        'python-gitlab>=3.4.0,<4',
    ],
    python_requires='>=3.10',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
    ],
)
